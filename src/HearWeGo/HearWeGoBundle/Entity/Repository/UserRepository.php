<?php

namespace HearWeGo\HearWeGoBundle\Entity\Repository;

use Doctrine\ORM\EntityRepository;

/**
 * UserRepository
 *
 * This class was generated by the Doctrine ORM. Add your own custom
 * repository methods below.
 */
class UserRepository extends EntityRepository
{
    public function findUserByEmail( $email ){
        $query  = $this->getEntityManager()->createQuery(
            'SELECT u FROM HearWeGoHearWeGoBundle:User u WHERE u.email = :email'
        )->setParameter('email', $email );
        return $query->getOneOrNullResult();
    }
}
